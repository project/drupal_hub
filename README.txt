
Drupal Hub

Created by Aaron Winborn
winborn@advomatic.com
Begun 16 January 2006

Drupal Hub is an api to allow a server to act as a portal to other web servers.
It associates a specific CCK content type with web listings, and optionally
allows the client servers to create and update their own listings, using xml.

------------
Installation
------------

1) Copy the drupal_hub folder into the modules directory of your Drupal 5.0 installation.
2) Go to the modules administration page and activate the desired modules. drupal_hub.module
   is required at a minimum.

Hub Server
----------

If you wish to act as a Drupal Hub server

3) Create a CCK type with the fields you wish to collect from other servers.
   At a minimum, you will need fields for the title (corresponds to site name),
   url, and description/mission.
4) Go to the Drupal Hub administration page and link the new type to your hub.
5) Link specific fields with the fields to collect information about.
6) Set the other configuration options as appropriate.

Hub Client
----------

If you wish your site to register with & update another specific Hub server

7) Fill in the information for each server you wish to notify.

-----
Usage
-----

Because this module is tied to CCK functionality, administrators of a Hub server may
use other modules to control listing functionality. For instance, they may use the
Views module to display listings as they wish, or they may create an event node type
with nodereference to allow clients to create events tied to their client servers.

As the module provides an API, other modules may extend the Hub module functionality.
For instance, RPG Hub will allow Hub servers to collect game statistics and information.

-----------------
Technical Details
-----------------

When the administrator of a client server enters the information of the hub server they
wish to register with, the client will request an xml document from the hub server. This
document will contain the information the hub server requires, plus a unique code.

After the administrator submits the required information, the information is stored in
the client server (and may be modified at any time). After any submission or modification,
the client again pings the hub server with the unique code.

After confirmation, the hub server will periodically check client servers, using the cron (and
a configurable time duration). It will request an xml document from the client, which must
contain both the unique identifier code and the other required information.

------------------------
Hub Server Configuration
------------------------

A Hub server will associate individual clients with nodes of a specific content type.
So first, an administrator must create and assign a node type to hub clients. They will
need, at a minimum, fields for the client site's title, url, and description. They may
assign any other fields as desired. From the Hub server administration page, the
administrator will then assign the node type for clients.

After this, when Hub clients have registered with the site, they will be available as a
paginated listing from the administration page, with various options available for mass
edits, such as ping, publish/unpublish, promote, or block.

Using the Views module, various listing pages and blocks can be created to harness the
power of the Hub module. If the Views module is enabled, some basic views will be made
available to the Views administration page.

------------------------
Hub Client Configuration
------------------------

After initially registering with a Hub server, a hub client will respond to periodic pings,
giving the status of that server, plus any special messages. An administrator may update
their information at any time from their admin screens, and the client will notify the Hub
server at the next ping that the info needs to be updated.

A Hub client may optionally display a block/page of information of the Hub servers they
are linked to. The Hub servers will send an html block to be displayed with a link. This
may, for instance, have a small image or brief description. This information will be
configurable by the client administrator.

-------------
Specification
-------------

All communication between the Hub server and client will use XML scripting. $clienthash and
$serverhash will be unique random variables created by each specific server. All communication will
be verified on both ends by checking these values.

Registration:
The Hub client (www.example.com) will access a page from the hub server (www.examplehub.com)
as follows:

http://www.examplehub.com/hub/xml/register?url=http://www.example.com&clienthash=xe3Fi8cPq914sQ5cORv3

In response, the Hub server will send an XML document similar to the following:

<xml>
  <clienthash>xe3Fi8cPq914sQ5cORv3</clienthash>
  <serverhash>ichEH83Cde492Ipz4H1d</serverhash>
  <huburl>http://www.examplehub.com</huburl>
  <hubversion>hub.module#1.0.72</hubversion>
  <fields>
    <title>
      <type>textfield</type>
      <title>Site Name</title>
      <default>site_name</default>
      <required>1</required>
    </title>
    <description>
      <type>textarea</type>
      <title>Site Description</title>
      <default>site_mission</default>
      <required>1</required>
    </description>
    <logo>
      <type>textfield</type>
      <title>Logo</title>
      <description>You may add a link to a logo for your site, which must be an image no larger
        than 80x120 pixels.</description>
      <required>0</required>
    </logo>
  </fields>
  <hubblock>
    <title>Example Hub</title>
    <logo>http://www.examplehub.com/files/hublogo.png</logo>
    <description>We're listed at the best Example hub ever!</description>
  </hubblock>
</xml>

After this, the Hub client administrator will now see a form (in a fieldset under the new Hub server)
on the administration page at /admin/hub/client. The form will look something like the following:

Hub Server: http://www.examplehub.com
Hub Server Version: hub.module#1.0.72

Site Name: *
  +-----------------------------+
  | Example dot com             |
  +-----------------------------+

Site Description: *
  +---------------------------------------+
  | This is the site mission for Example  |
  | dot com, the most used site for       |
  | example tutorials on the Internet!    |
  +---------------------------------------+

Logo:
  +---------------------------------------+
  | http://www.example.com/files/logo.jpg |
  +---------------------------------------+

Status:
  (Dropdown w/ following:
    Active
    Offline
  )

Hub Server Block:

  A new block will be created for you to optionally display. As currently configured, it will
  appear as follows:

  {=================}  Example Hub
  { H U B - L O G O }  We're listed at the best Example hub ever!
  {=================}

  Hub Title:
    +---------------------+
    | Example Hub         |
    +---------------------+

  Hub Logo:
    +---------------------------------------------+
    | http://www.examplehub.com/files/hublogo.png |
    +---------------------------------------------+

  Hub Description: *
    +---------------------------------------+
    | We're listed at the best Example hub  |
    | ever!                                 |
    |                                       |
    |                                       |
    +---------------------------------------+

+------+    +--------+
| Ping |    | Delete |  (NOTE: these buttons only appear after initially submitted.)
+------+    +--------+

This form will also have a #value of hub_client_hash_c_123 = xe3Fi8cPq914sQ5cORv3,
and #value of hub_client_hash_s_123 = ichEH83Cde492Ipz4H1d. (The _123 is a unique value chosen
by the client to correspond to this hub server, and is used only internally.)

After this, the Hub server will ping the Hub client for the new information, either when the Hub
client administrator hits the Ping button, or on a cron from either server. Note that the client
will not respond until the information has been reviewed and submitted by the administrator,
which it checks for with variable_get('hub_client_active_123', false).

Once the information has been submitted, the Hub server asks for the following document:
http://www.example.com/hub/xml/client/ping?hub=http://www.examplehub.com&serverhash=ichEH83Cde492Ipz4H1d

The Hub client responds with the following information:
<xml>
  <clienthash>xe3Fi8cPq914sQ5cORv3</clienthash>
  <hubversion>hub.module#1.0.58</hubversion>
  <status>1</status>
</xml>

Status may be:
0 = Offline or Not configured (this will be returned until the initial registration form has been submitted).
1 = Active

Once the status is 1, the Hub server will the request the client's registration information. (This may be
asked on other occassions, such as when the client pings with new information, or on a regular cron run.)

http://www.example.com/hub/xml/client/info?hub=http://www.examplehub.com&serverhash=ichEH83Cde492Ipz4H1d

The Hub client responds as follows:
<xml>
  <clienthash>xe3Fi8cPq914sQ5cORv3</clienthash>
  <hubversion>hub.module#1.0.58</hubversion>
  <status>1</status>
  <fields>
    <title>Example dot com</title>
    <description>This is the site mission for Example dot com, the most used site for example tutorials on the
      Internet!</description>
    <logo>http://www.example.com/files/logo.jpg</logo>
  </fields>
</xml>

At this point, the Hub server will create a node with the required information. The Hub server may then display
that information in various ways as configured by its adminstrator, most likely using Views and blocks, or
even simply promoting it to the front page.

-------------
Extensibility
-------------

The Drupal Hub module includes certain hooks, allowing for an API to be extended by other modules. For
instance, the RPG Hub module allows for game statistics to be automatically passed between the
Hub servers.