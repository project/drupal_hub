<?php

function drupal_hub_server_admin_settings() {
  $form = array();

  // Allow the admin to turn on or off all services offered by various Hub modules on the server.
  $form['cck'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => true,
    '#collapsed' => false,
  );

  $form['cck']['current'] = array(
    '#type' => 'fieldset',
    '#title' => t('Current Hub Listing Types'),
    '#description' => t('The following content types have been set to store Hub client listings. You may configure them individually below.'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $types = drupal_hub_server_type_load();
  if (empty($types)) {
    $form['cck']['current']['#description'] = t('No content types are currently configured to store Hub client listings. You may select one or more content types from below.');
  }
  $cck = content_types();
  $defaults = array(0 => t('(No Automatic Value)'));
  $defaults += module_invoke_all('hubapi', 'field defaults');
  foreach ($types as $type) {
    $form['cck']['current']['drupal_hub_server_type_' . $type['type']] = array(
      '#type' => 'fieldset',
      '#title' => $type['name'],
      '#description' => $type['description'],
      '#collapsible' => true,
      '#collapsed' => false,
    );
    $form['cck']['current']['drupal_hub_server_type_' . $type['type']]['auto'] = array(
      '#type' => 'fieldset',
      '#title' => t('Automatic Values'),
      '#description' => t('Please select the automatic values for each desired field here. When an automatic value has been selected, the client site will automatically enter its corresponding value of the appropriate variable.'),
      '#collapsible' => true,
      '#collapsed' => true,
    );
    if ($type['has_title']) {
      $form['cck']['current']['drupal_hub_server_type_' . $type['type']]['auto']['drupal_hub_server_' . $type['type'] . '_auto_title'] = array(
        '#type' => 'select',
        '#title' => $type['title_label'],
        '#options' => $defaults,
        '#default_value' => variable_get('drupal_hub_server_' . $type['type'] . '_auto_title', 'v:site_name'),
      );
    }
    if ($type['has_body']) {
      $form['cck']['current']['drupal_hub_server_type_' . $type['type']]['auto']['drupal_hub_server_' . $type['type'] . '_auto_body'] = array(
        '#type' => 'select',
        '#title' => $type['body_label'],
        '#options' => $defaults,
        '#default_value' => variable_get('drupal_hub_server_' . $type['type'] . '_auto_body', 'v:site_mission'),
      );
    }
    foreach($type['fields'] as $field) {
      $form['cck']['current']['drupal_hub_server_type_' . $type['type']]['auto']['drupal_hub_server_' . $type['type'] . '_auto_' . $field['field_name']] = array(
        '#type' => 'select',
        '#title' => $field['widget']['label'],
        '#options' => $defaults,
        '#default_value' => variable_get('drupal_hub_server_' . $type['type'] . '_auto_' . $field['field_name'], '0'),
      );
    }
  }

  $options = array();
  foreach ($cck as $type) {
    $options[$type['type']] = $type['name'];
  }
  $form['cck']['drupal_hub_server_cck_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add Hub Listing Types'),
    '#options' => $options,
    '#default_value' => variable_get('drupal_hub_server_cck_types', array()),
    '#description' => t('To enable a content type as a Hub listing type, check the box for the type and hit submit. You will then be able to modify its specific settings above. Likewise, to remove a type, uncheck the box and submit. It will then be removed from the listings above.'),
  );

  return system_settings_form($form);
}

function drupal_hub_server_type_load($type_name = NULL) {
  static $types = array();

  if (empty($types)) {
    $drupal_hub_types = variable_get('drupal_hub_server_cck_types', array());
    foreach ($drupal_hub_types as $type => $value) {
      if ($value) {
        $cck_type = content_types($value);
        $types[$cck_type['type']] = $cck_type;
      }
    }
  }
  if ($type_name) {
    return $types[$type_name];
  }
  return $types;
}

/**
 *  This is called by a client hub as an xmlrpc to request registration info.
 *  The server will respond with an xmlrpc message to the client with the appropriate info.
 */
function _drupal_hub_server_test($bool) {
  if ($bool) { $output = 'true'; } else { $output = 'false'; }
  return $output . t(' testing drupal_hub_server_register_request');
}

/**
 *  This returns the Hub server's info, in response to a ping from client servers
 *  wishing to list basic information about why they should register with a particular
 *  Hub server.
 */
function _drupal_hub_server_info() {
  return variable_get('drupal_hub_server_info', variable_get('site_mission', ''));
}

/**
 *  This xml function will return the descriptions of the cck listing types
 */
function _drupal_hub_server_register_info() {
  return drupal_hub_server_type_load();
}

/**
 * Implementation of hook_xmlrpc().
 * call like:
 *  $url = 'http://drupalhub.org/xmlrpc.php';
 *  $xml = xmlrpc($url, 'hub.server.register.request', $arg1, $arg2, ..., $argN);
 */
function _drupal_hub_server_xmlrpc() {
  // TODO
  $xmlrpc = array();
  $xmlrpc[] = array(
    'hub.server.test',
    '_drupal_hub_server_test',
    array('string', 'boolean'), // array($return_value_type, $arg1, $arg2, ..., $argN);
    t('Test Hub server'),
  );
  $xmlrpc[] = array(
    'hub.server.info',
    '_drupal_hub_server_info',
    array('string'),
    t('Request Hub server\'s info'),
  );
  $xmlrpc[] = array(
    'hub.server.register.info',
    '_drupal_hub_server_register_info',
    array('struct'),
    t('Request Hub server\'s info'),
  );
/*    $xmlrpc[] = array(
      'drupal.login',
      'drupal_login',
      array('int', 'string', 'string'),
      t('Logging into a Drupal site')
    );*/

  return $xmlrpc;
}

