<?php

function drupal_hub_client_admin_settings() {
  $form = array();

  // Allow the admin to turn on or off all services offered by various Hub modules on the server.
  $form['servers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Known Drupal Hub Servers'),
    '#description' => t('This is a list of known Drupal Hub Servers, and their last known status. You may add or delete a listing with them from here. Once you\'ve added and submitted the form, you may configure a server below.'),
    '#collapsible' => true,
    '#collapsed' => false,
  );
  $servers = module_invoke_all('hubapi', 'server defaults');
  foreach ($servers as $server => $info) {
    $form['servers'][$server] = array(
      '#type' => 'checkbox',
      '#title' => $info['name'],
      '#description' => xmlrpc($info['url_xmlrpc'], 'drupal_hub.server.info'),
      '#default_value' => variable_get($server, false),
    );
    if (variable_get($server, false)) {
      $form['config_' . $server] = array(
        '#type' => 'fieldset',
        '#title' => t('@name Configuration Options', array('@name' => $info['name'])),
        '#collapsible' => true,
        '#collapsed' => false,
      );
      $types = xmlrpc($info['url_xmlrpc'], 'drupal_hub.server.register.info');
      foreach($types as $type) {
        $form['config_' . $server][$server . '_type_' . $type['type']] = array(
          '#type' => 'checkbox',
          '#title' => $type['name'],
          '#description' => $type['description'],
          '#default_value' => variable_get($server . '_type_' . $type['type'], false),
        );
      }
    }
  }
  return system_settings_form($form);
}

